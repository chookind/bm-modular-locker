﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UIControl;
using System.Drawing;
using System.Data;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls.Primitives;
using BMModularLocker.Pages;

namespace BMModularLocker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow main;

        PageLogin pgLogin = new PageLogin();
        PageMain pgMain = new PageMain();
        PageSetting pgSetting = new PageSetting();

        public MainWindow()
        {
            InitializeComponent();
            main = this;

            HeaderTop.SetUser(Config.Cfg.AccessMode);
            HeaderTop.SetStatus("Login Page");
            MainFrame.Content = pgLogin;
            SetAccess(3);
            this.pgLogin.LoginSuccessSA += PgLogin_LoginSuccessSA;
            this.pgLogin.LoginSuccessA += PgLogin_LoginSuccessA;
            this.pgLogin.LogoutSuccess += pgLogin_LogoutSuccess;
        }

        private void PgLogin_LoginSuccessA(object sender, EventArgs e)
        {
            btnHome.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            pgSetting.AutoConnect();
            SetAccess(2);
        }

        private void PgLogin_LoginSuccessSA(object sender, EventArgs e)
        {
            btnHome.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            pgSetting.AutoConnect();
            SetAccess(3);
        }

        private void pgLogin_LogoutSuccess(object sender, EventArgs e)
        {
            btnLogin.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            pgSetting.AutoDisconnect();
            pgMain.ButtonEnable();
            pgMain.ButtonUnhide();
            SetAccess(1);
        }
        
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            HeaderTop.SetStatus("Exiting");
            MessageBoxResult result = MessageBox.Show("Are you sure to exit the application?", "Exit Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                HeaderTop.SetStatus("Exit");
                pgSetting.AutoDisconnect();
                System.Windows.Application.Current.Shutdown();
            }
            else
                HeaderTop.SetStatus("Main");
        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = pgLogin;
            HeaderTop.SetStatus("Login Page");
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            HeaderTop.SetUser(Config.Cfg.AccessMode);
            HeaderTop.SetStatus("Locker Cavity");
            MainFrame.Content = pgMain;
        }

        private void btnUser_Click(object sender, RoutedEventArgs e)
        {
            HeaderTop.SetStatus("User Management");
            //MainFrame.Content = pgUser;
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            HeaderTop.SetStatus("Setting");
            MainFrame.Content = pgSetting;
        }

        private void btnDataLog_Click(object sender, RoutedEventArgs e)
        {
            HeaderTop.SetStatus("Data Log");
            //MainFrame.Content = pgDataLog;

        }

        private void MainMenu_MouseLeave(object sender, MouseEventArgs e)
        {
            MainMenu.IsOpen = false;
        }

        private void btmBorder_MouseEnter(object sender, MouseEventArgs e)
        {
            btmBorder.Visibility = Visibility.Hidden;
            MainMenu.IsOpen = true;
        }

        private void MainMenu_ClosingFinished(object sender, RoutedEventArgs e)
        {
            btmBorder.Visibility = Visibility.Visible;
        }

        public void SetAccess(int level)
        {
            if (level == 1)
            {
                btnHome.IsEnabled = false;
                btnLogin.IsEnabled = true;
                btnSetting.IsEnabled = false;
                btnLogin.Tag = "LOGIN";
            }
            else if (level == 2)
            {
                btnHome.IsEnabled = true;
                btnLogin.IsEnabled = true;
                btnSetting.IsEnabled = false;
                pgMain.ButtonDisable();
                pgMain.ButtonHide();
                btnLogin.Tag = "LOGOUT";
            }
            else if (level == 3)
            {
                btnHome.IsEnabled = true;
                btnLogin.IsEnabled = true;
                btnSetting.IsEnabled = true;
                btnLogin.Tag = "LOGOUT";
            }
        }

        private void WinClosed(object sender, EventArgs e)
        {
            //  pgMain.onClose();
        }
    }
}
