﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMModularLocker.Pages;
using Ini;

namespace BMModularLocker
{
    class ShareVariable
    {
        public static string SettngsPath = Environment.CurrentDirectory + "\\Settings.ini";
        static IniFile ini = new IniFile(SettngsPath);

        public static string NetworkPath
        {
            get
            {
                //Console.WriteLine(Environment.CurrentDirectory);
                string path = ini.ReadValue(Section.Database, Key.ConnString);
                return path;
            }
            set
            {
                ini.WriteValue(Section.Database, Key.ConnString, value);
            }
        }

        public static string IP
        {
            get
            {
                string path = ini.ReadValue(Section.IPAddress, Key.Address);
                return path;
            }
            set
            {
                ini.WriteValue(Section.IPAddress, Key.Address, value);
            }
        }

        public static string PortNumber
        {
            get
            {
                string path = ini.ReadValue(Section.PortNum, Key.Port);
                return path;
            }
            set
            {
                ini.WriteValue(Section.PortNum, Key.Port, value);
            }
        }
    }

    public class Section
    {
        public static string Database = "Database";
        public static string SaveFileLocation = "SaveFileLocation";
        public static string IPAddress = "IPAddress";
        public static string PortNum = "PortNum";
    }

    public class Key
    {
        // Database
        public const string ConnString = "ConnString";
        public const string Location = "Location";
        public const string Address = "Address";
        public const string Port = "Port";
    }
}