﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace UIControl
{
    /// <summary>
    /// Interaction logic for Header.xaml
    /// </summary>
    public partial class Header : UserControl
    {
        public string currentUser;
        public Header()
        {
            InitializeComponent();
            //Initialize timer
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        public void SetUser(string user)
        {
            tboxLogin.Text = user;
            currentUser = user;
        }

        public void SetStatus(string status)
        {
            lblStatus.Content = status;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            tboxTime.Text = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
        }

       
    }
}
