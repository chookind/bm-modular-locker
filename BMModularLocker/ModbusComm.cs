﻿using EasyModbus;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SophicTemplate
{
    class ModbusComm
    {
        public ModbusClient modbusCommunication;

        public void StartModbus()
        {
            modbusCommunication = new EasyModbus.ModbusClient();
        }

        public void ConnectModbus(string UniqueIdentifier)
        {
            try
            {
                if (modbusCommunication.Connected)
                    modbusCommunication.Disconnect();

                modbusCommunication.SerialPort = Config.Cfg.SerialPort;
                modbusCommunication.UnitIdentifier = byte.Parse(UniqueIdentifier); //byte.Parse(Config.Cfg.SerialID.ToString());
                modbusCommunication.Baudrate = int.Parse(Config.Cfg.SerialBaudrate);
                modbusCommunication.Parity = (Parity)Enum.Parse(typeof(Parity), Config.Cfg.SerialParity);
                modbusCommunication.StopBits = (StopBits)Enum.Parse(typeof(StopBits), Config.Cfg.SerialStopBit);

                modbusCommunication.Connect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        public int ReadSingleData(int unique, int startaddress)
        {
            try
            {
                ConnectModbus(unique.ToString());

                int[] response = modbusCommunication.ReadInputRegisters(startaddress, 1);
                return response[0];

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return -1;
            }
        }

        public int[] ReadLouverBox(int unique, int address1, int address2, int address3, int address4)
        {
            int[] arrValue = new int[4];
            try
            {
                ConnectModbus(unique.ToString());
                int[] response = modbusCommunication.ReadInputRegisters(address1, 1);
                arrValue[0] = response[0];

                response = modbusCommunication.ReadInputRegisters(address2, 1);
                arrValue[1] = response[0];

                response = modbusCommunication.ReadInputRegisters(address3, 1);
                arrValue[2] = response[0];

                response = modbusCommunication.ReadInputRegisters(address4, 1);
                arrValue[3] = response[0];
                return arrValue;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                arrValue[0] = -1;
                arrValue[1] = -1;
                arrValue[2] = -1;
                arrValue[3] = -1;
                return arrValue;
            }
        }
    }

}
