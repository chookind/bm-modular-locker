﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMModularLocker.Pages;
using Ini;

namespace BMModularLocker
{
    class Config
    {
        private static volatile Config instance;
        private static object syncRoot = new Object();
        public IniFile ini = new IniFile(Environment.CurrentDirectory + "\\Settings.ini");

        public static Config Cfg
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Config();
                    }
                }
                return instance;
            }
        }

        public string AccessMode = "Normal";
    }
}