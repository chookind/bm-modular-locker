﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.Data;
using System.Windows.Threading;

namespace BMModularLocker.Pages
{
    /// <summary>
    /// Interaction logic for PageLogin.xaml
    /// </summary>
    public partial class PageLogin : Page
    {
        //If have staff database
        DataTable dt_Staff = new DataTable();
        SqlDbCmd sqlDbCmd = new SqlDbCmd();
        DispatcherTimer pingStaff = new DispatcherTimer();

        public PageLogin()
        {
            InitializeComponent();
            sqlDbCmd.GetConnString();

            pingStaff.Interval = TimeSpan.FromSeconds(5);
            pingStaff.Tick += pingStaff_Tick;
            pingStaff.Start();
        }

        public event EventHandler LoginSuccessSA;
        public event EventHandler LoginSuccessA;
        public event EventHandler LogoutSuccess;

        private void pingStaff_Tick(object sender, EventArgs e)
        {
            try
            {
                pingStaff.Stop();
                dt_Staff = sqlDbCmd.RetrieveData(string.Format("select * from Staff"));
                pingStaff.Start();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Content.ToString() == "LOG IN")
            {
                string id = user_tb.Text;
            }
            else if (btn.Content.ToString() == "LOG OUT")
            {
                //DataLog(string.Format("Insert into DataLog ([DateTime],[LoginActivity]) Values ('{0}','{1} (Admin) Has Log Out')", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.sss"), user_tb.Text));
                user_tb.Text = "";
                user_tb.IsEnabled = true;
                Config.Cfg.AccessMode = "Normal";
                btnLogin.Content = "LOG IN";
                pingStaff.Start();
                LogoutSuccess.Invoke(null, null);
            }
        }

        //If using scan badge to login
        private void textInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (user_tb.Text.Contains("\r\n"))
                {
                    string userInput = user_tb.Text.Replace("\r\n", "");
                    DataView checkUser = new DataView(dt_Staff, string.Format("StaffID = '{0}'", userInput), "StaffID Asc", DataViewRowState.CurrentRows);
                    if (checkUser.Count > 0)
                    {
                        string role = checkUser[0]["SystemRole"].ToString();
                        if (role.ToUpper().Equals("SUPERADMIN"))
                        {
                            WpfMessageBox.Show(string.Format("Welcome {0}", user_tb.Text), "Login Successful", MessageBoxButton.OK, MessageBoxImage.Information, 2);
                            user_tb.IsEnabled = false;
                            Config.Cfg.AccessMode = "Super Admin";
                            btnLogin.Content = "LOG OUT";
                            pingStaff.Stop();
                            LoginSuccessSA.Invoke(null, null);
                        }
                        else if(role.ToUpper().Equals("ADMIN"))
                        {
                            WpfMessageBox.Show(string.Format("Welcome {0}", user_tb.Text), "Login Successful", MessageBoxButton.OK, MessageBoxImage.Information, 2);
                            user_tb.IsEnabled = false;
                            Config.Cfg.AccessMode = "Admin";
                            btnLogin.Content = "LOG OUT";
                            pingStaff.Stop();
                            LoginSuccessA.Invoke(null, null);
                        }
                    }
                    else
                    {
                        user_tb.Clear();
                        user_tb.Focus();
                        WpfMessageBox.Show("Invalid Badge ID", "No Scanned Badge Data", MessageBoxButton.OK, MessageBoxImage.Warning, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                user_tb.Clear();
                user_tb.Focus();
                Logger.LogError(ex.ToString());
            }
        }

        private void pass_tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return")
                btnLogin.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            user_tb.Focus();
            dt_Staff = sqlDbCmd.RetrieveData(string.Format("select * from Staff"));
        }
    }
}
