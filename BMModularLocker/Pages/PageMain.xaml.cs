﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using Microsoft.Win32;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using BMModularLocker.UControl;

namespace BMModularLocker.Pages
{
    public partial class PageMain : Page
    {
        LockerConnection lockerConnection = new LockerConnection();
        SqlDbCmd sqlDbCmd = new SqlDbCmd();
        DataTable dt_Cus = new DataTable();
        DataTable dt_Adm = new DataTable();
        DataTable dt_DataLog = new DataTable();
        DataTable dt_AssignLocker = new DataTable();
        DataTable dt_AssignLockers = new DataTable();
        DispatcherTimer pingSql = new DispatcherTimer();
        DispatcherTimer pingLocker = new DispatcherTimer();
        DispatcherTimer updateData = new DispatcherTimer();
        DispatcherTimer updateStatus = new DispatcherTimer();
        DispatcherTimer deleteTick = new DispatcherTimer();
        DispatcherTimer deleteFirst = new DispatcherTimer();
        LockerCavityButtonUI[] ui;

        int[] ioMap = new int[4];
        DateTime dtNow;
        string strYear;
        string strMonth;
        string strDay;
        string fullDate;
        string query;
        private byte[] responseData1 = new byte[12];
        private byte[] responseData2 = new byte[12];
        string returnByte1;
        string returnByte2;
        string returnByte3;
        string returnByte4;
        string scannedUserID = "";
        string scannedLockerID = "";
        string scannedRandomCode = "";
        string scannedDate = "";
        string userInput = "";
        
        //string returnByte5;
        //string returnByte6;

        bool connectSQL = true;
        bool offline = false;
        bool firstLoad = true;
        bool firstDelete = false;

        int[,] colourSequence = new int[,] { {1, 1, 1, 1 }, { 0, 1, 1, 1 }, { 1, 0, 1, 1 }, { 0, 0, 1, 1 },
                                            {1, 1, 0, 1 }, {0, 1, 0, 1 }, {1, 0, 0, 1 }, {0, 0, 0, 1 },
                                            {1, 1, 1, 0 }, {0, 1, 1, 0 }, {1, 0, 1, 0 }, {0, 0, 1, 0 },
                                            {1, 1, 0, 0 }, {0, 1, 0, 0 }, {1, 0, 0, 0 }, {0, 0, 0, 0 }};
        public PageMain()
        {
            InitializeComponent();
            SetupUI();
        }

        #region User Control
        public void ButtonDisable()
        {
            btnLocker1.IsEnabled = false;
            btnLocker2.IsEnabled = false;
            btnLocker3.IsEnabled = false;
            btnLocker4.IsEnabled = false;
            btnLocker5.IsEnabled = false;
            btnLocker6.IsEnabled = false;
            btnLocker7.IsEnabled = false;
            btnLocker8.IsEnabled = false;
            btnLocker9.IsEnabled = false;
            btnLocker10.IsEnabled = false;
            btnLocker11.IsEnabled = false;
            btnLocker12.IsEnabled = false;
            btnLocker13.IsEnabled = false;
            btnLocker14.IsEnabled = false;
            btnLocker15.IsEnabled = false;
            btnLocker16.IsEnabled = false;
            btnLocker17.IsEnabled = false;
            btnLocker18.IsEnabled = false;
            btnLocker19.IsEnabled = false;
            btnLocker20.IsEnabled = false;
        }

        public void ButtonEnable()
        {
            btnLocker1.IsEnabled = true;
            btnLocker2.IsEnabled = true;
            btnLocker3.IsEnabled = true;
            btnLocker4.IsEnabled = true;
            btnLocker5.IsEnabled = true;
            btnLocker6.IsEnabled = true;
            btnLocker7.IsEnabled = true;
            btnLocker8.IsEnabled = true;
            btnLocker9.IsEnabled = true;
            btnLocker10.IsEnabled = true;
            btnLocker11.IsEnabled = true;
            btnLocker12.IsEnabled = true;
            btnLocker13.IsEnabled = true;
            btnLocker14.IsEnabled = true;
            btnLocker15.IsEnabled = true;
            btnLocker16.IsEnabled = true;
            btnLocker17.IsEnabled = true;
            btnLocker18.IsEnabled = true;
            btnLocker19.IsEnabled = true;
            btnLocker20.IsEnabled = true;
        }

        public void ButtonHide()
        {
            UnlockAll.Visibility = Visibility.Hidden;
        }

        public void ButtonUnhide()
        {
            UnlockAll.Visibility = Visibility.Visible;
        }
        #endregion

        private void SetupUI()
        {
            try
            {
                ui = new LockerCavityButtonUI[] { btnLocker1, btnLocker2, btnLocker3, btnLocker4, btnLocker5, btnLocker6, btnLocker7, btnLocker8, btnLocker9, btnLocker10, btnLocker11, btnLocker12, btnLocker13, btnLocker14, btnLocker15, btnLocker16, btnLocker17, btnLocker18, btnLocker19, btnLocker20 };
                foreach (LockerCavityButtonUI btn in ui)
                {
                    btn.Tag = string.Empty; //make sure each button tag has initialise
                }
                bool result = sqlDbCmd.GetConnString();
                if (!result)
                {
                    MessageBoxResult messageBoxResult = WpfMessageBox.Show("Network Connection Error", "Please check the network connection and reopen the program.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                GetAllTable();
                AssignLockerNumber();
                
                pingSql.Interval = TimeSpan.FromSeconds(5);
                pingSql.Tick += PingSql_Tick;
                pingSql.Start();

                pingLocker.Interval = TimeSpan.FromSeconds(5);
                pingLocker.Tick += PingLocker_Tick;
                pingLocker.Start();

                updateData.Interval = TimeSpan.FromSeconds(3);
                updateData.Tick += UpdateData_Tick;
                updateData.Start();

                updateStatus.Interval = TimeSpan.FromSeconds(1);
                updateStatus.Tick += UpdateStatus_Tick;
                updateStatus.Start();

                deleteTick.Interval = TimeSpan.FromHours(1);
                deleteTick.Tick += DeleteTick_Tick;
                //deleteTick.Start();

                deleteFirst.Interval = TimeSpan.FromSeconds(30);
                deleteFirst.Tick += DeleteTick_Tick;
                deleteFirst.Start();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        private void GetAllTable()
        {
            try
            {
                dtNow = DateTime.Now;
                strYear = dtNow.Year.ToString();
                strMonth = dtNow.Month.ToString();
                strDay = dtNow.Day.ToString();
                fullDate = string.Format("{0}-{1}-{2}", strYear, strMonth, strDay);

                dt_Cus = sqlDbCmd.RetrieveData(string.Format("select * from Customer"));
                dt_Adm = sqlDbCmd.RetrieveData(string.Format("select * from Internal"));
                dt_DataLog = sqlDbCmd.RetrieveData(string.Format("select * from DataLog"));
                dt_AssignLockers = sqlDbCmd.RetrieveData(string.Format("select * from AssignLocker"));
                dt_AssignLocker = sqlDbCmd.RetrieveData(string.Format("Select * from AssignLocker WHERE TakeDate >= '{0}' and TakeDate < DATEADD(DAY, 1, '{1}')", fullDate, fullDate));
                GetLockerCavityDetails();
                firstLoad = false;
            }
            catch (Exception ex)
            {
                if (!firstLoad)
                {
                    offline = true;
                    pingSql.Start();
                }
                Logger.LogError(ex.ToString());
            }
        }

        private void AssignLockerNumber()
        {
            query = string.Format("select * from LockerCavity");
            DataTable dt = sqlDbCmd.RetrieveData(query);
            int buttonTag = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                buttonTag++;
                ui[i].InitLockerCavityButtonUI(buttonTag);
            }
        }

        private void PingSql_Tick(object sender, EventArgs e)
        {
            try
            {
                pingSql.Stop();
                pingSql.Interval = TimeSpan.FromSeconds(2);
                connectSQL = sqlDbCmd.GetConnString();
                if (connectSQL)
                {
                    offline = false;
                    txtOffline.Background = Brushes.Lime;
                    txtOffline.Text = "Online";
                    textInput.IsReadOnly = false;
                    textInput.Focus();
                }
                else
                {
                    txtOffline.Background = Brushes.Red;
                    txtOffline.Text = "Offline";
                    textInput.IsReadOnly = true;
                    pingSql.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        private void PingLocker_Tick(object sender, EventArgs e)
        {
            try
            {
                pingLocker.Stop();
                if (lockerConnection.PingConnection())
                {
                    txtLocker.Background = Brushes.Lime;
                    txtLocker.Text = "Online";
                }
                else
                {
                    txtLocker.Background = Brushes.Red;
                    txtLocker.Text = "Offline";
                }
                pingLocker.Start();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        private void UpdateData_Tick(object sender, EventArgs e)
        {
            updateData.Stop();
            try
            {
                if (!offline)
                    GetAllTable();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
            updateData.Start();
        }

        private void UpdateStatus_Tick(object sender, EventArgs e)
        {
            updateStatus.Stop();
            try
            {
                if (!offline)
                    textInput.Focus();
                    GetLockerStatus();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
            updateStatus.Start();
        }

        private void DeleteTick_Tick(object sender, EventArgs e)
        {
            if (!firstDelete)
            {
                deleteFirst.Stop();
                firstDelete = true;
            }
            deleteTick.Stop();
            sqlDbCmd.ExecuteNonQuery(string.Format("UPDATE Internal SET Internal.Status = 0 FROM Internal INNER JOIN AssignLocker ON Internal.InternalID = AssignLocker.UserId AND AssignLocker.TakeDate < '{0}'", fullDate));
            sqlDbCmd.ExecuteNonQuery(string.Format("UPDATE Customer SET Customer.Status = 0 FROM Customer INNER JOIN AssignLocker ON Customer.CustomerId = AssignLocker.UserId AND AssignLocker.TakeDate < '{0}'", fullDate));
            sqlDbCmd.ExecuteNonQuery(string.Format("DELETE FROM AssignLocker WHERE QRCode = '-' AND TakeDate < '{0}'", fullDate));
            deleteTick.Start();
        }

        private void GetLockerStatus()
        {
            if (lockerConnection.SendByte(0x00, 0x00, 0x50, 0x55))
                responseData1 = lockerConnection.ReceiveByte();

            if(lockerConnection.SendByte(0x01, 0x00, 0x50, 0x56))
                responseData2 = lockerConnection.ReceiveByte();

            returnByte1 = responseData1[4].ToString("X2");
            returnByte2 = responseData1[5].ToString("X2");

            returnByte3 = responseData2[4].ToString("X2");
            returnByte4 = responseData2[5].ToString("X2");

            SetLockerStatus(returnByte1[1], returnByte1[0], 1, 0);
            SetLockerStatus(returnByte2[1], returnByte2[0], 2, 0);

            SetLockerStatus(returnByte3[1], returnByte3[0], 1, 1);
            SetLockerStatus(returnByte4[1], returnByte4[0], 2, 1);

        }

        private void SetLockerStatus(char receiveByte1, char receiveByte2, int num, int labelling)
        {
            try
            {
                for (int i = 0; i < 16; i++)
                {
                    string hexValue = i.ToString("X");
                    if (hexValue.Equals(receiveByte1.ToString()))
                    {
                        int hex = Int32.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
                        if (num == 1 && labelling == 0)
                        {
                            ui[0].BtnCavityChangeColour(colourSequence[hex, 0]);
                            ui[1].BtnCavityChangeColour(colourSequence[hex, 1]);
                            ui[2].BtnCavityChangeColour(colourSequence[hex, 2]);
                            ui[3].BtnCavityChangeColour(colourSequence[hex, 3]);
                        }
                        else if (num == 2 && labelling == 0)
                        {
                            ui[8].BtnCavityChangeColour(colourSequence[hex, 0]);
                            ui[9].BtnCavityChangeColour(colourSequence[hex, 1]);
                        }
                        else if (num == 1 && labelling == 1)
                        {
                            ui[10].BtnCavityChangeColour(colourSequence[hex, 0]);
                            ui[11].BtnCavityChangeColour(colourSequence[hex, 1]);
                            ui[12].BtnCavityChangeColour(colourSequence[hex, 2]);
                            ui[13].BtnCavityChangeColour(colourSequence[hex, 3]);
                        }
                        else if (num == 2 && labelling == 1)
                        {
                            ui[18].BtnCavityChangeColour(colourSequence[hex, 0]);
                            ui[19].BtnCavityChangeColour(colourSequence[hex, 1]);
                        }
                        break;
                    }
                }

                for (int i = 0; i < 16; i++)
                {
                    string hexValue = i.ToString("X");
                    if (hexValue.Equals(receiveByte2.ToString()))
                    {
                        int hex = Int32.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
                        if (num == 1 && labelling == 0)
                        {
                            ui[4].BtnCavityChangeColour(colourSequence[hex, 0]);
                            ui[5].BtnCavityChangeColour(colourSequence[hex, 1]);
                            ui[6].BtnCavityChangeColour(colourSequence[hex, 2]);
                            ui[7].BtnCavityChangeColour(colourSequence[hex, 3]);
                        }
                        else if (num == 1 && labelling == 1)
                        {
                            ui[14].BtnCavityChangeColour(colourSequence[hex, 0]);
                            ui[15].BtnCavityChangeColour(colourSequence[hex, 1]);
                            ui[16].BtnCavityChangeColour(colourSequence[hex, 2]);
                            ui[17].BtnCavityChangeColour(colourSequence[hex, 3]);
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        private void GetLockerCavityDetails()
        {
            for (int i = 0; i < ui.Length; i++)
            {
                ui[i].UsedLocker = 0;
            }

            foreach (DataRow row in dt_AssignLocker.Rows)
            {
                if (row["LockerCavityID"].ToString().Equals("LC10001"))
                    ui[0].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10002"))
                    ui[1].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10003"))
                    ui[2].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10004"))
                    ui[3].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10005"))
                    ui[4].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10006"))
                    ui[5].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10007"))
                    ui[6].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10008"))
                    ui[7].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10009"))
                    ui[8].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10010"))
                    ui[9].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10011"))
                    ui[10].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10012"))
                    ui[11].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10013"))
                    ui[12].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10014"))
                    ui[13].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10015"))
                    ui[14].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10016"))
                    ui[15].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10017"))
                    ui[16].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10018"))
                    ui[17].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10019"))
                    ui[18].BtnToolTip(row["UserID"].ToString());
                else if (row["LockerCavityID"].ToString().Equals("LC10020"))
                    ui[19].BtnToolTip(row["UserID"].ToString());
            }

            for (int i = 0; i < ui.Length; i++)
            {
                if (ui[i].UsedLocker == 0)
                    ui[i].EmptyToolTip();
            }
        }

        #region Process QR Code
        private void textInput_TextChanged(object sender, TextChangedEventArgs e)
        {

            try
            {
                if (textInput.Text.Contains("\r\n"))
                {
                    processUserInput();
                    textInput.IsReadOnly = true;
                    
                    if (dt_AssignLockers.Rows.Count > 0)
                    {
                        DataView checkAvailability = new DataView(dt_AssignLockers, "QRCode = '"+userInput+"'","QRCode asc",DataViewRowState.CurrentRows);
                        if (checkAvailability.Count > 0)
                        {
                            if (scannedDate.Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                            {
                                if (dt_Cus.Rows.Count > 0 && scannedUserID[0].ToString().Equals("C"))
                                {
                                    for (int i = 0; i <= dt_Cus.Rows.Count - 1; i++)
                                    {
                                        if (dt_Cus.Rows[i].Field<string>(0).ToString().Equals(scannedUserID) && dt_Cus.Rows[i].Field<string>(6).ToString().Equals(scannedRandomCode))
                                        {
                                            processCavityAndDBQuery(1);
                                        }
                                    }
                                }
                                else if (dt_Adm.Rows.Count > 0 && scannedUserID[0].ToString().Equals("I"))
                                {
                                    for (int i = 0; i <= dt_Adm.Rows.Count - 1; i++)
                                    {
                                        if (dt_Adm.Rows[i].Field<string>(0).ToString().Equals(scannedUserID) && dt_Cus.Rows[i].Field<string>(6).ToString().Equals(scannedRandomCode))
                                        {
                                            processCavityAndDBQuery(2);
                                        }

                                    }
                                }
                                else
                                {
                                    clearText();
                                    WpfMessageBox.Show("Invalid QR Code", "No matched user ID", MessageBoxButton.OK, MessageBoxImage.Warning, 2);
                                }
                            }
                            else
                            {
                                clearText();
                                WpfMessageBox.Show("Invalid QR Code", "Not today date", MessageBoxButton.OK, MessageBoxImage.Warning, 2);
                            }
                        }
                        else
                        {
                            clearText();
                            WpfMessageBox.Show("Invalid QR Code", "QR Code not found", MessageBoxButton.OK, MessageBoxImage.Warning, 2);
                        }
                        
                    }
                    else
                    {
                        clearText();
                        WpfMessageBox.Show("Invalid QR Code", "No Assigned Locker Data", MessageBoxButton.OK, MessageBoxImage.Warning, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                clearText();
                Logger.LogError(ex.ToString());
            }

        }
        private void processUserInput()
        {
            textInput.Text = textInput.Text.Trim(' ');
            userInput = textInput.Text.Replace("\r\n", "");
            string[] splitString = userInput.Split('|');
            scannedUserID = splitString[0];
            scannedLockerID = splitString[1];
            scannedRandomCode = splitString[2];
            scannedDate = splitString[3].Replace("-", "/");
        }

        public bool processCavityQuery()
        {
            DataTable dt_IOMaps = sqlDbCmd.RetrieveData(string.Format("Select * from IOMapping WHERE LockerCavityID = '{0}'", scannedLockerID));
            for (int j = 0; j < ioMap.Length; j++)
                ioMap[j] = (int)dt_IOMaps.Rows[0][j + 1];

            bool byteSend = lockerConnection.SendByte((byte)ioMap[0], (byte)ioMap[1], (byte)ioMap[2], (byte)ioMap[3]);
            dt_IOMaps.Dispose();

            return byteSend;
        }

        public void processDBQuery(bool byteSend, int userPosition)
        {
            if (byteSend)
            {
                int countDataLogID = 0;
                dtNow = DateTime.Now;
                string strsYear = dtNow.Year.ToString();
                string strsMonth = dtNow.Month.ToString();
                string strsDay = dtNow.Day.ToString();
                string strHours = dtNow.Hour.ToString();
                string strMinutes = dtNow.Minute.ToString();
                string strSeconds = dtNow.Second.ToString();
                string formatDate = string.Format("{0}-{1}-{2} {3}:{4}:{5}", strsYear, strsMonth, strsDay, strHours, strMinutes, strSeconds);

                for (int j = 0; j <= dt_DataLog.Rows.Count - 1; j++)
                {
                    countDataLogID++;
                }
                string newDataLogID = generateDataLogID((countDataLogID + 1).ToString());
                

                List<string> allQuery = new List<string>();
                allQuery.Add("INSERT INTO DataLog (DataLogID, LockerCavityID, UserID, VisitDateTime, Activity) VALUES ('" + newDataLogID + "','" + scannedLockerID + "', '" + scannedUserID + "', '" + formatDate + "','" + getLockerCavityStatus(scannedLockerID) + "')");
                allQuery.Add("UPDATE AssignLocker SET QRCode = '-' WHERE QRCode = '" + userInput + "'");

                sqlDbCmd.ExecuteTransaction(allQuery);

                clearText();
                WpfMessageBox.Show("Open Success", getLockerCavityStatus(scannedLockerID), MessageBoxButton.OK, MessageBoxImage.Information, 2);
            }
            else
            {
                clearText();
                WpfMessageBox.Show("Open Failed", "Cavity open failed", MessageBoxButton.OK, MessageBoxImage.Information, 2);
            }
        }

        public void processCavityAndDBQuery(int userPosition)
        {
            //process cavity query
            bool byteSend = processCavityQuery();


            //process DB query
            processDBQuery(byteSend, userPosition);
        }

        public void clearText()
        {
            textInput.IsReadOnly = false;
            textInput.Clear();
            textInput.Focus();
        }

        public string generateDataLogID(string newCountDataLogID)
        {
            while (newCountDataLogID.Length <= 4)
            {
                newCountDataLogID = "0" + newCountDataLogID;
            }
            string newDataLogID = "DL" + newCountDataLogID;
            return newDataLogID;
        }

        public string getLockerCavityStatus(string lockerID)
        {
            int currentCavity = Convert.ToInt32(lockerID.GetLast(2));
            return string.Format("Cavity " + currentCavity + " Opened");
        }

        #endregion

        private void UnlockAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = WpfMessageBox.Show("Unlock All Locker Confirmation", "Are you sure want to unlock all locker?", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    bool byteSend = lockerConnection.SendByte(0x0A, 0x30, 0x51, 0x90);
                    if (!byteSend)
                    {
                        WpfMessageBox.Show("Error", "An issue happen on door controller, please contact technician for help or check the wiring", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            } 
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }
    }
}
