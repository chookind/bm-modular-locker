﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BMModularLocker.Pages
{
    /// <summary>
    /// Interaction logic for PageSetting.xaml
    /// </summary>
    public partial class PageSetting : Page
    {
        LockerConnection lockerConnection = new LockerConnection();
        SqlDbCmd sqlDb = new SqlDbCmd();
        string query;

        string start;
        string addr;
        string lockNum;
        string command;
        string end;
        string total;

        private static readonly Regex _regex = new Regex("[^0-9]+");
        public static bool disconnect = true; //False = Connect, True = Disconnect

        public PageSetting()
        {
            InitializeComponent();
            sqlDb.GetConnString();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                query = string.Format("Select distinct LockerCavityID from IOMapping");
                dt = sqlDb.RetrieveData(query);
                comboBoxLocker.ItemsSource = dt.DefaultView;
                comboBoxLocker.DisplayMemberPath = dt.Columns["LockerCavityID"].ToString();
                comboBoxLocker.SelectedIndex = 0;

                if (disconnect == true)
                {
                    AutoConnect();
                } 
                else
                {
                    Description.Content = "Client Socket Program - \nServer Already Connected Automatically";
                }
            } 
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        public void AutoConnect()
        {
            try
            {
                if (lockerConnection.SocketConnect(ShareVariable.IP, Int32.Parse(ShareVariable.PortNumber)))
                {
                    Description.Content = "Client Socket Program - \nServer Successful Connected Automatically";

                    Img.Source = new BitmapImage(new Uri("/Resources/disconnect_button.PNG", UriKind.Relative));
                    ConDiscon.Text = "Disconnect";
                    IPAddress.Text = ShareVariable.IP;
                    Port.Text = ShareVariable.PortNumber;
                    IPAddress.IsEnabled = false;
                    Port.IsEnabled = false;
                    disconnect = false;
                }
                else
                {
                    Description.Content = "Server connect failed";
                    disconnect = true;
                    WpfMessageBox.Show("Error", "Cannot connect to the control board \nCheck the wiring or contact the technician", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            } 
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                Description.Content = "Server connect failed";
                disconnect = true;
                WpfMessageBox.Show("Error", "Cannot connect to the control board \n Check the wiring or contact the technician", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void AutoDisconnect()
        {
            try
            {
                if (!disconnect)
                {
                    if (lockerConnection.SocketDisconnect())
                    {
                        Img.Source = new BitmapImage(new Uri("/Resources/connect_button.png", UriKind.Relative));
                        ConDiscon.Text = "Connect";

                        Description.Content = "Successfully disconnect";
                        IPAddress.IsEnabled = true;
                        Port.IsEnabled = true;
                        disconnect = true;
                    }
                    else
                    {
                        Description.Content = "Error occured while trying to disconnect";
                        disconnect = false;
                    }
                }
            } 
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                Description.Content = "Error occured while trying to disconnect";
                disconnect = false;
            }
        }

        private void Label_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1)
            {
                var label = (Label)sender;
                Keyboard.Focus(label.Target);
            }
        }

        //Connect the client to the server
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            if (ConDiscon.Text.Equals("Connect"))
            {
                try
                {
                    if (lockerConnection.SocketConnect(IPAddress.Text, Int32.Parse(Port.Text)))
                    {
                        Description.Content = "Client Socket Program - \nServer Successful Connected Manually";

                        Img.Source = new BitmapImage(new Uri("/Resources/disconnect_button.PNG", UriKind.Relative));
                        ConDiscon.Text = "Disconnect";
                        IPAddress.IsEnabled = false;
                        Port.IsEnabled = false;
                        disconnect = false;
                    }
                    else
                    {
                        Description.Content = "Server connect failed";
                        disconnect = true;
                    }
                }
                catch (Exception ex)
                {
                    Description.Content = "Server connect failed";
                    disconnect = true;
                    Logger.LogError(ex.ToString());
                }
            }
            else if (ConDiscon.Text.Equals("Disconnect"))
            {
                try
                {
                    if (lockerConnection.SocketDisconnect())
                    {
                        Img.Source = new BitmapImage(new Uri("/Resources/connect_button.png", UriKind.Relative));
                        ConDiscon.Text = "Connect";

                        Description.Content = "Successfully disconnect";
                        IPAddress.IsEnabled = true;
                        Port.IsEnabled = true;
                        disconnect = true;
                    }
                    else
                    {
                        Description.Content = "Error occured while trying to disconnect";
                        disconnect = false;
                    }
                } 
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    Description.Content = "Error occured while trying to disconnect";
                    disconnect = false;
                }
            }
        }

        private void comboBoxLocker_DropDownClosed(object sender, EventArgs e)
        {
            Description.Content = "";
            DataTable dt = new DataTable();
            query = string.Format("select * from IOMapping where LockerCavityID = '{0}'", comboBoxLocker.Text);
            dt = sqlDb.RetrieveData(query);

            if (dt.Rows.Count == 0)
            {
                WpfMessageBox.Show("No record found", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
                ioMapSettings.ItemsSource = null;
                dt.Dispose();
            }
            else
            {
                dt.Columns.Add("Start", typeof(Int32), "2");
                dt.Columns.Add("End", typeof(Int32), "3");

                start = dt.Rows[0]["Start"].ToString();
                addr = dt.Rows[0]["Addr"].ToString();
                lockNum = dt.Rows[0]["LockNum"].ToString();
                command = dt.Rows[0]["Command"].ToString();
                end = dt.Rows[0]["End"].ToString();
                total = dt.Rows[0]["Total"].ToString();

                ioMapSettings.ItemsSource = dt.DefaultView;
                dt.Dispose();
            }
        }

        private void IOMap_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            TextBox t = e.EditingElement as TextBox;

            if (IsTextAllowed(t.Text.ToString()))
            {
                if (e.Column.Header.ToString().Equals("Addr"))
                {
                    addr = t.Text.ToString();
                }
                else if (e.Column.Header.ToString().Equals("LockNum"))
                {
                    lockNum = t.Text.ToString();
                }

                total = (Int32.Parse(start) + Int32.Parse(addr) + Int32.Parse(lockNum) + Int32.Parse(command) + Int32.Parse(end)).ToString();
                labelSuccessUnsuccess.Content = "The total is " + total;
            }
            else
            {
                MessageBoxResult messageBoxResult = WpfMessageBox.Show("Error", "Only Integer are allowed", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }

        private void updateIOMap_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = WpfMessageBox.Show("Are you sure?", "Update Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    query = string.Format("update IOMapping set Addr = '{0}', LockNum = '{1}', Command = '{2}', Total = '{3}' where LockerCavityID = '{4}'", addr, lockNum, command, total, comboBoxLocker.Text);
                    sqlDb.ExecuteNonQuery(query);
                    labelSuccessUnsuccess.Content = "IO Mapping update successfully";
                    MessageBoxResult messageBoxResults = WpfMessageBox.Show("Success", "IO Mapping update successfully", MessageBoxButton.OK, MessageBoxImage.Information);
                    Logger.LogIO(query);
                }
            }
            catch (Exception ex)
            {
                labelSuccessUnsuccess.Content = "IO Mapping update failed";
                MessageBoxResult messageBoxResults = WpfMessageBox.Show("Failed", "IO Mapping update failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.LogError(ex.ToString());
            }
        }
    }
}
