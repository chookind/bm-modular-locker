﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BMModularLocker
{
    class Logger
    {
        public static void LogError(string strData)
        {   //This will create new folder . ErrorLog is the Folder name
            string dicPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\LocalLog\ErrorLogs";
            WriteToLog(strData, dicPath, "Error.txt");
        }

        public static void LogTestResult(string strData, string filename)
        {   //This will create new folder . ErrorLog is the Folder name
            string dicPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\LocalLog\TestResultLogs";// + @"\" + filename; 
            WriteToLog(strData, dicPath, filename);
        }

        public static void LogIO(string strData)
        {
            //This will create new folder . ErrorLog is the Folder name
            string dicPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\LocalLog\IOLogs";
            WriteToLog(strData, dicPath, "IOLog.txt");
        }

        public static void LogTestResultRecord(string strData)
        {   //This will create new folder . ErrorLog is the Folder name
            string dicPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\LocalLog\TestResultRecords";// + @"\" + filename; 
            WriteToLog(strData, dicPath, "TestResultTransferLog.txt");
        }

        private static void WriteToLog(string DataLog, string DirectoryLog, string filename)
        {
            System.IO.StreamWriter objWriter;
            DateTime dtNow = DateTime.Now;
            string strYear = dtNow.Year.ToString();
            string strMonth = dtNow.Month.ToString();
            string strDay = dtNow.Day.ToString();
            string FolderName = string.Format("{0}_{1}_{2}", strYear, strMonth, strDay);
            string TimeDate = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
            DirectoryLog = DirectoryLog + "\\" + FolderName + "\\" + "PreWeight";
            //string dicPath = Environment.SpecialFolder.MyDocuments + "\\eScrapLog\\UpdatedLog";
            //Check if the directory exists
            if (Directory.Exists(DirectoryLog) == false)
            {
                Directory.CreateDirectory(DirectoryLog);
            }
            //Can change file type
            string fullPath = DirectoryLog + "\\" + filename;

            //Try catch to prevent program hang if file is open
            try
            {
                // Write content
                objWriter = new System.IO.StreamWriter(fullPath, true);
                objWriter.Write(TimeDate + "\n" + DataLog + "\n\n");
                objWriter.Close();
            }
            catch (Exception Ex)
            {

            }
        }

        private static void WriteToLogOverWrite(string DataLog, string DirectoryLog)
        {
            System.IO.StreamWriter objWriter;
            DateTime dtNow = DateTime.Now;
            string strYear = dtNow.Year.ToString();
            string strMonth = dtNow.Month.ToString();
            string strDay = dtNow.Day.ToString();
            string FileName = string.Format("{0}_{1}_{2}", strYear, strMonth, strDay);
            //string dicPath = Environment.SpecialFolder.MyDocuments + "\\eScrapLog\\UpdatedLog";
            //Check if the directory exists
            if (Directory.Exists(DirectoryLog) == false)
            {
                Directory.CreateDirectory(DirectoryLog);
            }
            //Can change file type
            string fullPath = DirectoryLog + "\\" + FileName + ".csv";

            //Try catch to prevent program hang if file is open
            try
            {
                // Write content
                objWriter = new System.IO.StreamWriter(fullPath, false);
                objWriter.Write(DataLog);
                objWriter.Close();
            }
            catch (Exception Ex) { }
        }
    }
}
