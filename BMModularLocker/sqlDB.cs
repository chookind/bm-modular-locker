﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace BMModularLocker
{
    class SqlDbCmd
    {
        public string connString;

        public DataTable RetrieveData(string SQLStr)
        {
            SqlConnection Conn = new SqlConnection(connString);

            SqlCommand SqlCmd = default(SqlCommand);
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            try
            {
                if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }
                SqlCmd = new SqlCommand(SQLStr, Conn);
                dt.Clear();
                da = new SqlDataAdapter(SqlCmd);
                da.Fill(dt);

                SqlCmd.Dispose();
                da.Dispose();

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dt.Dispose();
                Conn.Close();
                Conn.Dispose();
            }
        }

        public bool ExecuteNonQuery(string strQuery)
        {
            SqlConnection conn = new SqlConnection(connString);
            //SqlConnection conn = new SqlConnection("Data Source=" + System.Net.Dns.GetHostName() + ";Initial Catalog=laundry_tracking;User ID=sa;Password=administrator21");

            SqlCommand cmd = new SqlCommand();

            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                cmd.Connection = conn;
                cmd.CommandText = strQuery;
                if (cmd.ExecuteNonQuery() != -1)
                {
                    cmd.Dispose();
                    return true;
                }
                else
                {
                    cmd.Dispose();
                    return false;
                }

            }
            catch (Exception ex)
            {
                //log error
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                conn.Close();
            }
        }

        public string ReplaceStr(string Command)
        {
            return Command.Trim().Replace("'", "''");
        }

        public bool GetConnString()
        {
            connString = ShareVariable.NetworkPath;

            try
            {
                using (var connection = new SqlConnection(connString))
                {
                    connection.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false; // any error is considered as db connection error for now
            }
        }

        public void ExecuteTransaction(List<string> Query)
        {
            SqlConnection conn = new SqlConnection(connString);

            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                using (SqlTransaction sqlTransaction = conn.BeginTransaction("insertTransactionTable"))
                {
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = sqlTransaction;
                            foreach (string subquery in Query)
                            {
                                cmd.CommandText = subquery;
                                cmd.ExecuteNonQuery();
                            }
                            cmd.Dispose();
                            sqlTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        sqlTransaction.Rollback();
                        throw new Exception("Fail to save these changes into database, please make sure your inputs are correct." + "\r\n" + ex.Message);
                    }
                    finally
                    {
                        sqlTransaction.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //log error
                throw new Exception(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }

    class SqlDB
    {
        private string constring;
        SqlConnection sql;

        public SqlDB()
        {
            constring = "Data Source = sophicauto.ddns.net; Initial Catalog = sls; User Id = izzati; Password = izzati; Connection Timeout=1";
            sql = new SqlConnection(constring);
        }
        public void Connect()
        {
            if (sql.State == ConnectionState.Closed) sql.Open();
        }
        public void Disconnect()
        {
            if (sql.State == ConnectionState.Open) sql.Close();
        }

        public Task<DataTable> GetDataSetAsync(string SQLStr)
        {
            try
            {
                return Task.Run(() =>
                {
                    Connect();
                    DataTable dt = new DataTable();
                    SqlCommand sqlcmd = new SqlCommand(SQLStr, sql);
                    SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
                    sda.Fill(dt);
                    dt.Dispose();
                    Disconnect();
                    return dt;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void ExecuteNonQuery(string SQL)
        {
            try
            {
                Connect();
                SqlCommand sqlcmd = new SqlCommand(SQL, sql);
                sqlcmd.ExecuteNonQuery();
                sqlcmd.Dispose();
                Disconnect();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool GetConnString()
        {
            try
            {
                sql.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public DataTable RetrieveData(string query)
        {
            DataTable dt = new DataTable();
            Connect();
            SqlCommand sqlcmd = new SqlCommand(query, sql);
            SqlDataAdapter sqlad = new SqlDataAdapter(sqlcmd);
            sqlad.Fill(dt);
            sqlad.Dispose();
            Disconnect();
            dt.Dispose();
            return dt;
        }
    }
}


