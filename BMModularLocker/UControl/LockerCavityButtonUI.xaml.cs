﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasyModbus;

namespace BMModularLocker.UControl
{
    /// <summary>
    /// Interaction logic for LockerCavityButtonUI.xaml
    /// </summary>
    public partial class LockerCavityButtonUI : UserControl
    {
        LockerConnection lockerConnection = new LockerConnection();
        SqlDbCmd sqlDbCmd = new SqlDbCmd();

        int[] ioMap = new int[4];
        int usedLocker = 0;

        public int UsedLocker
        {
            get
            {
                return usedLocker;
            }
            set
            {
                usedLocker = value;
            }
        }

        DateTime dtNow;
        string strYear;
        string strMonth;
        string strDay;
        string fullDate;

        public LockerCavityButtonUI()
        {
            InitializeComponent();
            sqlDbCmd.GetConnString();
        }

        public void InitLockerCavityButtonUI(int buttonTag)
        {
            try
            {
                Tag = buttonTag.ToString();
                LockerCavityNum.Text = buttonTag.ToString();
                lockerToolTipNum.Text = "Locker Cavity " + buttonTag.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int lockerID = Int32.Parse(LockerCavityNum.Text);
                DataTable dt = sqlDbCmd.RetrieveData(string.Format("Select * from IOMapping where LockerCavityID = '{0}'", "LC1" + lockerID.ToString("0000")));
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < ioMap.Length; i++)
                        ioMap[i] = (int)dt.Rows[0][i + 1];

                    bool byteSend = lockerConnection.SendByte((byte)ioMap[0], (byte)ioMap[1], (byte)ioMap[2], (byte)ioMap[3]);

                    if (byteSend)
                    {
                        BtnCavityChangeColour(1);
                    }
                    else
                    {
                        WpfMessageBox.Show("Error", "An issue happen on door controller, please contact technician for help or check the wiring", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        public void BtnCavityChangeColour(int decider)
        {
            if (decider == 0)
            {
                this.Resources["BackgroundColorCavityA"] = Color.FromArgb(255, 235, 14, 14);
                this.Resources["BackgroundColorCavityB"] = Color.FromArgb(255, 217, 61, 43);
                this.Resources["BackgroundColorCavityC"] = Color.FromArgb(255, 226, 52, 52);
                btnLockerCavityTxt.Text = "Lock";
                LockNUnlock.Source = new BitmapImage(new Uri("/Resources/lock.png", UriKind.Relative));
            }
            else if (decider == 1)
            {
                this.Resources["BackgroundColorCavityA"] = Color.FromArgb(255, 23, 106, 21);
                this.Resources["BackgroundColorCavityB"] = Color.FromArgb(255, 32, 141, 37);
                this.Resources["BackgroundColorCavityC"] = Color.FromArgb(255, 25, 118, 63);
                btnLockerCavityTxt.Text = "Unlock";
                LockNUnlock.Source = new BitmapImage(new Uri("/Resources/unlock.png", UriKind.Relative));
            }
        }

        public void BtnToolTip(string custID)
        {
            try
            {
                DataTable dt_SelectedCustomer = sqlDbCmd.RetrieveData(string.Format("Select * from Customer WHERE CustomerID = '{0}'", custID));
                if (dt_SelectedCustomer.Rows.Count > 0)
                {
                    usedLocker = 1;

                    dtNow = DateTime.Now;
                    strYear = dtNow.Year.ToString();
                    strMonth = dtNow.Month.ToString();
                    strDay = dtNow.Day.ToString();
                    fullDate = string.Format("{0}-{1}-{2}", strYear, strMonth, strDay);

                    DataTable dt_SelectedAssignLocker = sqlDbCmd.RetrieveData(string.Format("Select * from AssignLocker WHERE UserID = '{0}' AND QRCode = '-' AND (TakeDate >= '{1}' and TakeDate < DATEADD(DAY, 1, '{2}'))", custID, fullDate, fullDate));

                    usedBy.Text = dt_SelectedCustomer.Rows[0]["Name"].ToString();
                    company.Text = dt_SelectedCustomer.Rows[0]["Company"].ToString();
                    email.Text = dt_SelectedCustomer.Rows[0]["Email"].ToString();
                    hpNum.Text = dt_SelectedCustomer.Rows[0]["MobileNo"].ToString();

                    if (dt_SelectedAssignLocker.Rows.Count > 0)
                    {
                        qrStatus.Text = "QR Code Used";
                    } 
                    else
                    {
                        qrStatus.Text = "QR Code not Used";
                    }
                }
                dt_SelectedCustomer.Dispose();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        public void EmptyToolTip()
        {
            try
            {
                usedBy.Text = "N/A";
                company.Text = "N/A";
                email.Text = "N/A";
                hpNum.Text = "N/A";
                qrStatus.Text = "N/A";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }
    }
}
