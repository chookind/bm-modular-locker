using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;


namespace Ini
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        // Gathers a list of EntryKey for the known SectionHeader 
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string Section, int Key, string Value, [MarshalAs(UnmanagedType.LPArray)] byte[] Result, int Size, string FileName);
        // Gathers a list of SectionHeaders.
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(int Section, string Key, string Value, [MarshalAs(UnmanagedType.LPArray)] byte[] Result, int Size, string FileName);
        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <param name="INIPath"></param>
        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <param name="Section"></param>
        /// Section name
        /// <param name="Key"></param>
        /// Key Name
        /// <param name="Value"></param>
        /// Value Name
        public void WriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="Path"></param>
        /// <returns></returns>
        public string ReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();

        }


        /// <summary>
        ///  The Function called to obtain the SectionHeaders, and returns them in an Dynamic Array.
        /// </summary>
        /// <returns></returns>

        public string[] GetSectionNames()
        {
            //	Sets the maxsize buffer to 500, if the more is required then doubles the size each time.
            for (int maxsize = 500; true; maxsize *= 2)
            {
                //	Obtains the information in bytes and stores them in the maxsize buffer (Bytes array)
                byte[] bytes = new byte[maxsize];
                int size = GetPrivateProfileString(0, "", "", bytes, maxsize, path);

                // Check the information obtained is not bigger than the allocated maxsize buffer - 2 bytes.
                // if it is, then skip over the next section so that the maxsize buffer can be doubled.
                if (size < maxsize - 2)
                {
                    // Converts the bytes value into an ASCII char. This is one long string.
                    string Selected = Encoding.ASCII.GetString(bytes, 0, size - (size > 0 ? 1 : 0));
                    // Splits the Long string into an array based on the "\0" or null (Newline) value and returns the value(s) in an array
                    return Selected.Split(new char[] { '\0' });
                }
            }
        }

        /// <summary>
        ///  The Function called to obtain the EntryKey's from the given SectionHeader string passed and returns them in an Dynamic Array
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public string[] GetEntryNames(string section)
        {
            //	Sets the maxsize buffer to 500, if the more is required then doubles the size each time. 
            for (int maxsize = 500; true; maxsize *= 2)
            {
                //	Obtains the EntryKey information in bytes and stores them in the maxsize buffer (Bytes array).
                //	Note that the SectionHeader value has been passed.
                byte[] bytes = new byte[maxsize];
                int size = GetPrivateProfileString(section, 0, "", bytes, maxsize, path);

                // Check the information obtained is not bigger than the allocated maxsize buffer - 2 bytes.
                // if it is, then skip over the next section so that the maxsize buffer can be doubled.
                if (size < maxsize - 2)
                {
                    // Converts the bytes value into an ASCII char. This is one long string.
                    string entries = Encoding.ASCII.GetString(bytes, 0, size - (size > 0 ? 1 : 0));
                    // Splits the Long string into an array based on the "\0" or null (Newline) value and returns the value(s) in an array
                    return entries.Split(new char[] { '\0' });
                }
            }
        }

        /// <summary>
        /// The Function called to obtain the EntryKey Value from the given SectionHeader and EntryKey string passed, then returned
        /// </summary>
        /// <param name="section"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        public object GetEntryValue(string section, string entry)
        {
            //	Sets the maxsize buffer to 250, if the more is required then doubles the size each time. 
            for (int maxsize = 250; true; maxsize *= 2)
            {
                //	Obtains the EntryValue information and uses the StringBuilder Function to and stores them in the maxsize buffers (result).
                //	Note that the SectionHeader and EntryKey values has been passed.
                StringBuilder result = new StringBuilder(maxsize);
                int size = GetPrivateProfileString(section, entry, "", result, maxsize, path);
                if (size < maxsize - 1)
                {
                    // Returns the value gathered from the EntryKey
                    return result.ToString();
                }
            }
        }
    }
}
