﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using EasyModbus;

namespace BMModularLocker
{
    class LockerConnection
    {
        public static TcpClient clientSocket = new TcpClient();
        private byte[] ByteBuffer = new byte[10]; //Initialize byte array
        public static NetworkStream stream;

        public bool SocketConnect(string ipAddress, int port)
        {
            //Connect the Socket to the TCP Client
            try
            {
                clientSocket.Connect(ipAddress, port);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return false;
            }
            
        }

        public bool SocketDisconnect()
        {
            //Disconnect the Socket
            clientSocket.Client.Disconnect(true);

            if (clientSocket.Connected)
                return false;
            else
            {
                clientSocket = new TcpClient();
                return true;
            }
        }

        public bool PingConnection()
        {
            if (clientSocket.Connected)
                return true;
            else
                return false;
        }

        //Send the byte to the server
        public bool SendByte(byte addr, byte locknum, byte cmd, byte sum)
        {
            try
            {
                ByteBuffer[0] = 0x02;
                ByteBuffer[1] = addr;
                ByteBuffer[2] = locknum;
                ByteBuffer[3] = cmd;
                ByteBuffer[4] = 0x03;
                ByteBuffer[5] = sum;
                stream = clientSocket.GetStream();
                stream.Write(ByteBuffer, 0, ByteBuffer.Length);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return false;
            }
        }

        //Receive the byte responded from the server
        public byte[] ReceiveByte()
        {
            byte[] responseData = new byte[12];
            int bytesRead = stream.Read(responseData, 0, 12);

            return responseData;
        }
    }
}
